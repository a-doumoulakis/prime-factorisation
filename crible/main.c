#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gmp.h>
#include <pari/pari.h>
#include <omp.h>
#include "./../src/RSA.h"
#define PARI_SIZE 1000000
#define TAILLE_BUFF 58981
#define TAILLE_BUFF2 10000


long* prime_list(){
  /*TODO : Permetre à l'utilisateur d'entrer lui meme le fichier texte et de definir la taille des buffers*/
  FILE* file = fopen("liste premier.txt", "r");
  char* buffer = malloc(TAILLE_BUFF * sizeof(char));
  long* result = malloc(TAILLE_BUFF2 * sizeof(long)); 
  char* token;
  int i = 0;
  fgets(buffer, TAILLE_BUFF, file);
  token = strtok(buffer, " ");
  result[i++] = atol(token);
  while(token != NULL){
    token = strtok(NULL, " ");
    if(token){
      result[i++] = atol(token);
    }
  }
  free(buffer);
  fclose(file);
  return result;
}


long get_max_base(){
  return 2300;//choisir la taille maximum du plus grand nombre de la base  <------------------------------------------------------------- ici
}

long kronecker1(mpz_t a, long p){
  long po = (p - 1) / 2;
  long re;
  mpz_t rez, pre, pow;
  mpz_inits(rez, pre, pow, NULL);
  mpz_set_ui(pre, p);
  mpz_set_ui(pow, po);
  mpz_powm(rez, a, pow, pre);
  re = mpz_get_ui(rez);
  mpz_clears(rez, pre, pow, NULL);
  return re;
}

void polyn(mpz_t m, mpz_t y, mpz_t n, mpz_t resultat){
  mpz_t res;
  mpz_init(res);
  mpz_add(res, m, y);
  mpz_powm_ui(res, res, 2, n);
  mpz_set(resultat, res);
  mpz_clear(res);
}

int fact(mpz_t z, long* prime_list, int taille, int* res){
  int i = 0;
  int accept = 0;
  int cmp = 0;
  int size_res = 0;
  //int* res = malloc(sizeof(int) * taille);
  mpz_t r, q, temp;
  mpz_inits(r, q, temp,NULL);
  mpz_set_ui(r, 42);
  mpz_set(q, z);
  mpz_set(temp, z);
  while(mpz_cmp_ui(q, 1) != 0){
    mpz_cdiv_qr_ui(temp, r, q, prime_list[i]);
    if(mpz_cmp_ui(r, 0) != 0){
      break;
    }
    mpz_set(q, temp);
    cmp++;
  }
  res[size_res] = cmp;
  size_res++;
  cmp = 0;
  i++;
  while(prime_list[i]){
    while(mpz_cmp_ui(q, 1) != 0){
      mpz_cdiv_qr_ui(temp, r, q, prime_list[i]);
      if(mpz_cmp_ui(r, 0) != 0){
	break;
      }
      mpz_set(q, temp);
      cmp++;
    }
    if(cmp != 0){
      accept++;
    }
    res[size_res] = cmp;
    size_res++;
    cmp = 0;
    i++;
  }
  mpz_clears(r, q, temp ,NULL);
  if(accept > 0 && mpz_cmp_ui(r,0) == 0){
    return 0;
  }
  return 1;
}

void affiche_tab(int* table, long* list, int taille){
  int i = 0;
  while(i < taille - 1){
    if(table[i] != 0){
      printf("%ld^%i * ", list[i],table[i]); 
    }
    i++;
  }
  if(table[i] != 0){
    printf("%ld^%i", list[taille-1], table[taille-1]);
  }
  printf("\n");
}

long* get_base(long* list, mpz_t n){
  int i = 0;
  int j = 0;
  long base = get_max_base();
  long* res = malloc(sizeof(long) * 2);
  res[j++] = 2;
  while(j != 2){
    //TODO if(kronecker(n, p) == 1) ajoute a la liste et j++ i++
    if(mpz_kronecker_ui(n, list[i]) == 1){
      j++;
      res = (long*)realloc(res, j * sizeof(long));
      res[j-1] = list[i];
    }
    i++;
  }
  while(res[j-1] <= base){
    if(mpz_kronecker_ui(n, list[i]) == 1){
      j++;
      res = (long*)realloc(res, j * sizeof(long));
      res[j-1] = list[i];
    }
    i++; 
  }
  res = (long*)realloc(res, (j+1) * sizeof(long));
  res[j] = '\0';
  return res;
}

int* test_module(int base, mpz_t n){
  mpz_t foo;
  mpz_init(foo);
  int* res = malloc(sizeof(int) * 2);
  int i_foo, i;
  int j = 0;
  mpz_mod_ui(foo, n, base);
  i_foo = mpz_get_ui(foo);
  int tmp = 0;
  //printf("base %i\n foo %i\n",base, i_foo);
  for(i = 1; i < base; i++){
    tmp = i * i;
    if(tmp % base == i_foo){
      res[j++] = i;
      //printf("dans la base %i ajoute %i\n",base ,i);
    }
    if(j == 2){
      return res;
    }
  }
  mpz_clear(foo);
  return NULL;
}

/*begin http://rosettacode.org/wiki/Chinese_remainder_theorem */
// returns x where (a * x) % b == 1
int mul_inv(int a, int b){
  int b0 = b, t, q;
  int x0 = 0, x1 = 1;
  if (b == 1) return 1;
  while (a > 1) {
    q = a / b;
    t = b, b = a % b, a = t;
    t = x0, x0 = x1 - q * x0, x1 = t;
  }
  if (x1 < 0) x1 += b0;
  return x1;
}
/*
  int chinese_remainder(int n1, int n2, int c0, int c1){
  int p, prod = 1, sum = 0;
  prod = n1 * n2;
  p = prod / n1;
  sum += c0 * mul_inv(p, n1) * p;
  p = prod / n2;
  sum += c1 * mul_inv(p, n2) * p;
	
  return sum % prod;
  }*/

int chinese_remainder(int *n, int *a, int len)
{
  int p, i, prod = 1, sum = 0;
 
  for (i = 0; i < len; i++) prod *= n[i];
 
  for (i = 0; i < len; i++) {
    p = prod / n[i];
    sum += a[i] * mul_inv(p, n[i]) * p;
  }
 
  return sum % prod;
}

/*end*/

int main(){
  mpz_t n, resultat, m, m_n, y, y_private, y_chinese, modul1, modul2;
  int mod1, mod2;
  mpz_inits(n, resultat, m, m_n, y, y_chinese, modul1, modul2, NULL);
  int i = 0;
  int j = 0;
  int k = 0;
  int b1 = 0, b2 = 0;
  int accept = 0;
  int id, taille, nb_thread, sstop;
  int* list_fact = NULL;
  int* res1;
  int* res2;
  int chinese[4];
  int chinese_prod;
  double start, end;
  long* res = NULL;
  long* list = prime_list();
  int a[2];
  int n1[2];

  start = getCPUTime();
  nb_thread = omp_get_max_threads();
  omp_set_num_threads(nb_thread);
  printf("Il y a %i threads disponible qui vont être utilisés\n", nb_thread);
  mpz_set_str(n, "20133726993791131291", 10);
  mpz_sqrt(m,n);
  mpz_sub(m_n, n, m);
  gmp_printf("m = %Zd\n", m);
  res = get_base(list, n);
  printf("Base des nombres premier choisis : ");
  while(res[i]){
    //printf("%ld ",res[i]);
    i++;
  }
  taille = i;
  printf("\nTaille : %i", taille);
  printf("\n");
  mpz_mod_ui(modul1, n, res[2]);
  mpz_mod_ui(modul2, n, res[3]);
  b1 = res[1];
  b2 = res[2];
  mod1 = mpz_get_ui(modul1);
  mod2 = mpz_get_ui(modul2);
  i = 0;
  res1 = test_module(b1, n);
  if(res1 == NULL){
    printf("Fatal error de la mort qui tue res 1\n");
    exit(EXIT_FAILURE);
  }
  res2 = test_module(b2, n);
  if(res2 == NULL){
    printf("Fatal error de la mort qui tue res 2\n");
    exit(EXIT_FAILURE);
  }
  chinese_prod = b1 * b2;
  n1[0] = b1;
  n1[1] = b2;
  //printf("Base fin %i\n", chinese_prod);
  k = 0;
  for(i = 0; i < 2; i++){
    for(j = 0; j < 2; j++){
      a[0] = res1[i];
      a[1] = res2[j];
      //chinese[i] = chinese_remainder(b1, b2, res1[i], res2[j]);
      chinese[k++] = chinese_remainder(n1, a, 2);
      //printf("b1 : %i  b2 : %i res1 : %i res2 : %i\n",b1,b2 ,res1[i], res2[j]);
    }
  }
  printf("c1 = %i c2 = %i c3 = %i c4 = %i\n\n", chinese[0], chinese[1], chinese[2], chinese[3]);
  mpz_set_ui(y, 0);
  accept = 0;
  sstop = 0;
  mpz_t comp_c;
  mpz_init(comp_c);
  mpz_set_ui(comp_c, 0);
#pragma omp parallel private(id, list_fact, y_private)
  {
    list_fact =  malloc(sizeof(int) * taille);
    id = omp_get_thread_num();
    mpz_init(y_private);
    mpz_set_ui(y_private, 0);
    while(!sstop){
      //#pragma omp critical
      //{
      /*if(id == 0){
	mpz_add_ui(y_private, y_private, chinese[0]);
	//mpz_set(y_private, y);
	mpz_add_ui(y,y,1);
      }
      if(id == 1){
	mpz_add_ui(y_private, y_private, chinese[1]);
	//mpz_set(y_private, y);
	mpz_add_ui(y,y,1);
      }
      if(id == 2){
	mpz_add_ui(y_private, y_private, chinese[2]);
	//mpz_set(y_private, y);
	mpz_add_ui(y,y,1);
      }
      if(id == 3){
	mpz_add_ui(y_private, y_private, chinese[3]);
	//mpz_set(y_private, y);
	mpz_add_ui(y,y,1);
      }*/
      mpz_add_ui(y_private, y_private, 1);
      //}
      mpz_mod_ui(y_chinese, y_private, chinese_prod);
      polyn(m, y_private, n, resultat);
      mpz_add_ui(comp_c, comp_c, 1);
      if(fact(resultat, res, taille, list_fact) == 0){
	#pragma omp critical
	{
	  //printf("Thread %i => ", id);
	  //gmp_printf("y = %Zd | f(y) = %Zd \n\n", y_private, resultat);
	  //affiche_tab(list_fact, res, taille);
	  //printf("Nombre de decompositions trouvées : %i", accept);
	  //printf("\n\n");
	  accept++;
	  }
      }
      if(accept >= taille + 1 || mpz_cmp(y, m_n) == 0){
	sstop = 1;
#pragma omp flush(sstop)
      }
      //printf("Thread %d, sstop=%d ",id ,sstop);
      //gmp_printf("y = %Zd\n", y);      
    }
    free(list_fact);
    mpz_clear(y_private);
  }
  if(accept >= taille + 1){
    end = getCPUTime();
    printf("Base de facteur de taille adéquate nombre de decomposition : %i | taille %i\n", accept, taille);    
    printf("Temps utilisé : %lf s\n", (end - start));
    gmp_printf("nombre : %Zd, y = %Zd\n", comp_c, y);
  }
  else{
    printf("Base inadequat\n");
  }
  free(list_fact);
  free(list);
  free(res);
  mpz_clears(n, resultat, m, m_n, y, y_chinese, modul1, modul2, NULL);
  return 0;
}
