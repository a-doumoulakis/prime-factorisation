\select@language {french}
\contentsline {chapter}{Table des mati{\`e}res}{2}
\contentsline {chapter}{\chapternumberline {1}Algorithme Na\"{i}f}{4}
\contentsline {section}{\numberline {1.1}\'Etude de l'algorithme}{4}
\contentsline {subsection}{Explication}{4}
\contentsline {subsection}{Complexit\'{e}}{5}
\contentsline {section}{\numberline {1.2}Test de l'algorithme}{5}
\contentsline {subsection}{Environnement de test et m\'{e}thodologie}{5}
\contentsline {subsection}{R\'{e}sultats des calculs}{5}
\contentsline {chapter}{\chapternumberline {2}M\'{e}thode du Rho de Pollard}{8}
\contentsline {section}{\numberline {2.1}\'{E}tude de l'algorithme}{8}
\contentsline {subsection}{Explication}{8}
\contentsline {subsection}{Optimisation de l'algorithme}{9}
\contentsline {subsection}{Complexit\'{e}}{9}
\contentsline {section}{\numberline {2.2}Test de l'algorithme}{10}
\contentsline {subsection}{Environnement de test et m\'{e}thodologie}{10}
\contentsline {subsection}{R\'{e}sultats des calculs}{10}
\contentsline {chapter}{\chapternumberline {3}Algorithme du crible quadratique}{13}
\contentsline {section}{\numberline {3.1}\'Etude de l'algorithme}{13}
\contentsline {subsection}{Explication}{13}
\contentsline {subsubsection}{Objectif de l'algorithme}{14}
\contentsline {subsubsection}{Phase de collecte de donn\'{e}es}{14}
\contentsline {subsubsection}{Phase d'exploitation des donn\'{e}es}{15}
\contentsline {subsection}{Optimisation avec les reste chinois}{15}
\contentsline {subsection}{Complexit\'{e}}{16}
\contentsline {section}{\numberline {3.2}Test de l'algorithme}{16}
\contentsline {subsection}{Tests et m\'{e}thodologie}{16}
\contentsline {subsection}{R\'{e}sultats des tests}{17}
\contentsline {section}{\numberline {3.3}Sources}{19}
\contentsline {section}{\numberline {3.4}Programme}{20}
