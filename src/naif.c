#include <stdio.h>
#include "RSA.h"

double algNaif(mpz_t n, mpz_t facteur_premier, int opt_o, int t){
  double startTime, endTime; 
  if(t){
    startTime = getCPUTime();
  }
  mpz_t p, root, remain, cmp;
  int res = mpz_probab_prime_p(n, 25);
  mpz_inits(root, p, remain, cmp, NULL);
  if(res == 2){
    printf("n est premier\n");
    return 0;
  }
  mpz_sqrt(root, n);
  mpz_set_ui(p, 2);
  while(mpz_cmp(p, root) <= 0){
    mpz_cdiv_r(remain, n, p);
    //gmp_printf("division de %Zd / %Zd , reste = %Zd\n", n, p, remain);
    //printf(".");
    if(mpz_cmp_ui(remain,0) == 0){
      mpz_set(facteur_premier, p);
      if(t){
	endTime = getCPUTime();
	printf("CPU time used = %lf s\n", (endTime - startTime) );
      }
      gmp_printf("Nombre d'itérations : %Zd\n", cmp);
      gmp_printf("Facteur premier trouvé : %Zd\n", facteur_premier);
      mpz_clears(p, root, remain, cmp, NULL);
      if(t){
	return (endTime - startTime);
      }
      return 0;
    }
    if(opt_o == 0){
      mpz_nextprime(p, p);
    }
    else{
      if(mpz_cmp_ui(p,2) == 0){
	mpz_add_ui(p, p, 1);
      }
      else{
	mpz_add_ui(p, p, 2);
      }
    }
    mpz_add_ui(cmp, cmp, 1);
  }
  printf("Sortie de la boucle sans avoir trouvé de facteur premier, n est premier\n");
  gmp_printf("Nombre d'itérations = %Zd\n", cmp);
  if(t){
    endTime = getCPUTime();
    printf("CPU time used = %lf s\n", (endTime - startTime) );
  }
  mpz_clears(p, root, remain, cmp, NULL);
  if(t){
    return (endTime - startTime);
  }
  return 0;
}

void naif(struct RSA* RSA){
  mpz_t n, rez;
  double tot = 0;
  int repet, incr, ligne, i, nb;
  if(RSA -> l_option){
    repet = RSA -> l_option;
    printf("Vous avez demandé %i repetitions\n", repet);
  }
  else repet = 1;
  incr = 2;
  if(RSA -> d_option){
  ligne = RSA -> d_option;
  printf("Début des entiers choisi ligne %i du fichier\n", ligne);
  }
  else ligne = 3;
  mpz_inits(n, rez, NULL);
  printf("\n<--------- Debut des calculs --------->\n\n");
  for(i = 0; i < repet; i++){
    file_to_mpz(*(RSA -> argv), 10, ligne, n);
    ligne += incr;
    nb = mpz_sizeinbase(n, 10);
    gmp_printf("n = %Zd\n", n);
    printf("Nombre de chiffres dans n : %i\n", nb);
    tot += algNaif(n, rez, RSA -> o_option, RSA -> t_option);
    printf("\n\n");
  }
  printf("<--------- Fin des calculs --------->\n\n");
  tot /= repet;
  printf("Moyenne du temps CPU utilisé pour le calcul : %lf s\n", tot);
  mpz_clears(n, rez, NULL);
}
