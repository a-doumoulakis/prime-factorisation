//
//  RSA.h
//  RSA
//
//  Created by Wintermute on 07/03/2015.
//  Copyright (c) 2015 Wintermute. All rights reserved.
//

#define DEBUG
#ifndef  DEBUG
#define ASSERT(n)
#else
#define ASSERT(n)				\
  if(!(n)){					\
    printf ("%s - fail ", #n);			\
    printf ("On %s ",__DATE__);			\
    printf ("At %s ",__TIME__);			\
    printf ("In File %s ",__FILE__);		\
    printf ("At Line %i ",__LINE__);		\
    exit(1);}
#endif

#ifndef __RSA__RSA__
#define __RSA__RSA__

#include <gmp.h>

struct RSA{
  char *chdir;
    
  int argc_orig;
  char **argv_orig;
    
  int argc;
  char **argv;
    
  char *optarg;
    
  int mode;
  char *modestr;
    
  int t_option;
  int o_option;
  int l_option;
  int d_option;
  int a_option;
  int b_option;
};


//util.c
int Getopt(struct RSA *RSA);
long* prime_list();

//rho.c
void rho(struct RSA *RSA);

//naif.c
void naif(struct RSA *RSA);

//crible.c
void crible(struct RSA *RSA);

//file_to_mpz.c
void file_to_mpz(char* fic_name, unsigned int base, unsigned int no_line, mpz_t mp_num);

//getCPUtime.c
double getCPUTime();

#endif /* defined(__RSA__RSA__) */
