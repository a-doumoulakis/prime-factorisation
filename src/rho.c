//
//  naif.c
//  RSA
//
//  Created by Wintermute on 07/03/2015.
//  Copyright (c) 2015 Wintermute. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include "RSA.h"

double algRho(mpz_t n, mpz_t a, mpz_t x0, mpz_t rez, int opt_t, int opt_o, int t){
  double startTime, endTime;
  int iter = 0;
  if(opt_t){
    startTime = getCPUTime();
  }
  mpz_t compteur, x , y, temp, A, compteur_pgcd;
  mpf_t a1, b;
  if(opt_o){
    mpz_init(A);
    mpz_set_ui(A, 1);
  }
  mpz_inits(compteur, x, y, temp, compteur_pgcd, NULL);
  mpf_inits(a1, b, NULL);
  mpz_set(x,x0);//x<-x0
  mpz_set(y,x0);
    
  mpz_set_ui(rez,1);
  while(mpz_cmp_ui(rez,1) == 0){
    mpz_mul(temp,x,x);
    mpz_add(temp,temp,a);
    mpz_mod(x,temp,n);//x<-x^2+a mod n (f(x))
    mpz_mul(temp,y,y);
    mpz_add(temp,temp,a);
    mpz_mod(y,temp,n);//y<-y^2+a mod n
    mpz_mul(temp,y,y);
    mpz_add(temp,temp,a);
    mpz_mod(y,temp,n);//y<-y^2+a mod n (f(f(y))
    mpz_sub(temp,x,y);
    if(opt_o){
      mpz_mul(A, A, temp);
      mpz_mod(A, A, n);
      if(iter >= t){
	mpz_gcd(rez, A, n);
	mpz_add_ui(compteur_pgcd, compteur_pgcd,1);
	iter = 0;
      }
      else{
	iter++;
      }
    }
    else{
      mpz_gcd(rez,temp,n);// rez<-pgcd(x-y,n)
      mpz_add_ui(compteur_pgcd,compteur_pgcd,1);
    }
    
    mpz_add_ui(compteur,compteur,1);
  }
    
  if(opt_t){
    endTime = getCPUTime();
    printf("CPU time used = %lf s\n", (endTime - startTime));
  }
  mpf_set_z(a1, rez);
  mpf_sqrt(a1, a1);
  mpf_set_z(b, compteur);
  mpf_div(a1, b, a1);
  gmp_printf ("Nombre d'itérations : %Zd\n", compteur);
  gmp_printf("Iterations / sqrt(resultat) : %.*Ff \n", 15, a1);
  gmp_printf("Nombre de PGCD calculés : %Zd\n", compteur_pgcd);
  if(mpz_cmp(rez,n) == 0){
    gmp_printf ("Valeur de rez %Zd\n", rez);
    printf("Facteur non trouvé\n");
  }
  else{
    gmp_printf("Facteur premier trouvé : %Zd\n", rez);
  }
  mpz_clears(x, y, temp, compteur, NULL);
  mpf_clears(a1, b, NULL);
  if(opt_t){
    return (endTime - startTime);
  }
    return 0;
}


void rho(struct RSA* RSA){
  mpz_t n, a, x0, rez;
  double tot = 0;
  int repet, incr, ligne, i, nb;
  if(RSA -> l_option){
    repet = RSA -> l_option;
    printf("Vous avez demandé %i repetitions\n", repet);
  }
  else repet = 1;
  incr = 2;
  if(RSA -> d_option){
    ligne = RSA -> d_option;
    printf("Début des entiers choisi ligne %i du fichier\n", ligne);
  }
  else ligne = 3;
  mpz_inits(n,a,x0,rez,NULL);
  mpz_set_ui(x0,2);
  if(RSA -> a_option){
    mpz_set_ui(a,RSA -> a_option);
  }
  printf("\n<--------- Debut des calculs --------->\n\n");
  for(i = 0; i < repet; i++){
    file_to_mpz(*(RSA -> argv), 10, ligne, n);
    nb = mpz_sizeinbase(n, 10);
    ligne += incr;
    gmp_printf("n = %Zd\n", n);
    printf("Nombre de chiffres dans n : %i\n", nb);
    gmp_printf("Valeur de a : %Zd\n", a);
    tot += algRho(n, a, x0, rez, RSA -> t_option, RSA -> o_option, nb);
    printf("\n\n");
 }
  printf("<--------- Fin des calculs --------->\n\n");
  tot /= repet;
  if(RSA -> o_option){
    printf("Moyenne du temps CPU utilisé pour le calcul : %lf s\n", tot);
  }
  mpz_clears(n, a, x0, rez, NULL);
}
