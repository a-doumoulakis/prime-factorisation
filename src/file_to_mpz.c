#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "RSA.h"

void file_to_mpz(char* fic_name, unsigned int base, unsigned int no_line, mpz_t mp_num){
  int i = 0;
  int err = -1;
  int read;
  FILE* fd = fopen(fic_name, "r");
  char* line = NULL;
  size_t len = 0;
  if(base < 2 || base > 63){
    printf("La base entrée n'est pas supportée");
    exit(1);
  }
  if(fd == NULL){
    printf("Impossible d'ouvrir le fichier %s, errno : %i\n",fic_name, errno);
    fprintf(stderr, "file_to_mpn@file_to_limp : Ouverture du fichier échouée errno : %i\n", errno);
    exit(1);
  }
  for(i = 0; i< no_line; i++){
    read = getline(&line, &len, fd);
    if(read == -1){
      printf("La lecture du fichier a échouée à la ligne %i\n", i);
      fprintf(stderr, "file_to_mpn@file_to_limp : Lecture du fichier échouée errno : %i\n", errno);
      exit(1);
    }
  }
  /*TODO verifier que le texte lu est bien dans la bonne base*/
  line[read-1] = '\0';
  /*printf("le nombre lu est le suivant : %s\nDe longeur %i, dernier char %c \n",line, read, line[read-2]);*/
  err = mpz_set_str(mp_num, line, base);
  //printf("err : %i\n",err);
  free(line);
  fclose(fd);
  if(err == -1){
    printf("L'affectation de l'entier a échouée\n");
    exit(1);
  }
}
