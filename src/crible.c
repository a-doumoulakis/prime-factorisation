#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>
#include <pthread.h>
#include <time.h>
#include "RSA.h"

long* base_nb_premiers;
int sstop;
int accept;
mpz_t racine_n;
mpz_t n;

struct arg_struc{
  int depart;
  int taille_base;
  int prod_chinois;
  int id;
};

/*
struct imag{
  mpz_f r;
  mpz_f i;
};*/


long* get_base(long* list, mpz_t n, long max_base){
  int i = 0;
  int j = 0;
  mpz_t tmp;
  long* res = malloc(sizeof(long) * 2);
  mpz_init(tmp);
  res[j++] = 2;
  while(list[i] <= max_base){
    mpz_set_ui(tmp, list[i]);
    if(mpz_legendre(n, tmp) == 1){
      j++;
      res = (long*)realloc(res, j * sizeof(long));
      res[j-1] = list[i];
    }
    i++;
  }
  res = (long*)realloc(res, (j+1) * sizeof(long));
  res[j] = '\0';
  return res;
}

int fact(mpz_t a_fact, int taille, int* res){
  int i = 0, cmp = 0;
  mpz_t quotien, reste, temp, sqrt;
  mpz_inits(quotien, reste, temp, sqrt, NULL);
  mpz_sqrt(sqrt, a_fact);
  mpz_set(quotien, a_fact);
  while(i < taille){
    /*if(mpz_cmp_ui(quotien, base_nb_premiers[i]) < 0 || mpz_cmp_ui(sqrt, base_nb_premiers[i]) < 0){
      mpz_clears(quotien, reste, temp, NULL);
      printf("plop");
      return 1;
      }*/
    while(mpz_cmp_ui(quotien, 1) != 0){
      mpz_cdiv_qr_ui(temp, reste, quotien, base_nb_premiers[i]);
      if(mpz_cmp_ui(reste, 0) != 0){
	break;
      }
      mpz_set(quotien, temp);
      cmp++;
    }    
    res[i] = cmp;
    if(mpz_cmp_ui(quotien, 1) == 0 && mpz_cmp_ui(reste, 0) == 0){
      mpz_clears(quotien, reste, temp, NULL);
      return 0;
    }
    i++;
    cmp = 0;
  }
  mpz_clears(quotien, reste, temp, NULL);
  return 1;
}

int* test_module(int base, mpz_t n){ //renvoie un tableau de 2 Ã©lÃ©ments i et j infÃ©rieurs Ã  base telle que iÂ² mod base = jÂ² mod base = n mod base
  mpz_t foo;
  mpz_init(foo);
  int* res = malloc(sizeof(int) * 2);
  int i_foo, i;
  int j = 0;
  mpz_mod_ui(foo, n, base);
  i_foo = mpz_get_ui(foo);
  int tmp = 0;
  //printf("base %i\n foo %i\n",base, i_foo);
  for(i = 1; i < base; i++){
    tmp = i * i;
    if(tmp % base == i_foo){
      res[j++] = i;
      //printf("dans la base %i ajoute %i\n",base ,i);
    }
    if(j == 2){
      return res;
    }
  }
  mpz_clear(foo);
  return NULL;
}

/*begin http://rosettacode.org/wiki/Chinese_remainder_theorem */
// returns x where (a * x) % b == 1
int mul_inv(int a, int b){
  int b0 = b, t, q;
  int x0 = 0, x1 = 1;
  if (b == 1) return 1;
  while (a > 1) {
    q = a / b;
    t = b, b = a % b, a = t;
    t = x0, x0 = x1 - q * x0, x1 = t;
  }
  if (x1 < 0) x1 += b0;
  return x1;
}

int chinese_remainder(int *n, int *a, int len){
  int p, i, prod = 1, sum = 0;
 
  for (i = 0; i < len; i++) prod *= n[i];
 
  for (i = 0; i < len; i++) {
    p = prod / n[i];
    sum += a[i] * mul_inv(p, n[i]) * p;
  }
  return sum % prod;
}
/*end*/


void affiche_tab(int* table, long* list, int taille){
  int i = 0;
  while(i < taille - 1){
    if(table[i] != 0){
      printf("%ld^%i * ", list[i],table[i]); 
    }
    i++;
  }
  if(table[i] != 0){
    printf("%ld^%i", list[taille-1], table[taille-1]);
  }
  printf("\n");
}

void polyn(mpz_t m, mpz_t y, mpz_t n, mpz_t resultat){
  mpz_t res, m_x;
  mpz_inits(res, m_x, NULL);
  mpz_add(m_x, y, m);
  mpz_mul(res, m_x, m_x);
  mpz_sub(res, res, n);
  mpz_set(resultat, res); // f(x) = (m + x)² - n
  mpz_clears(res, m_x, NULL);
}


void boucle_facteurs(int iter, int taille_base, mpz_t sqrt_n, mpz_t n, int increment, int id){
  mpz_t y, resultat;
  int percent = -1;
  int* liste_facteurs = (int*)calloc(taille_base, sizeof(int));
  //memset(liste_facteurs, '0', sizeof(int) * taille_base);
  mpz_inits(y, resultat, NULL);
  mpz_set_ui(y, iter);
  mpz_set_ui(resultat, 0);
  while(!sstop){
    polyn(sqrt_n, y, n, resultat);
    //printf("Thread %i => ", id);
    //gmp_printf("Test pour y = %Zd\n\n", y);
    if(fact(resultat, taille_base, liste_facteurs) == 0){
      accept++;
      //printf("Thread %i => ", id);
      //gmp_printf("y = %Zd | f(y) = %Zd \n\n", y, resultat);
      //affiche_tab(liste_facteurs, base_nb_premiers, taille_base);
      percent = (int)((accept * 100)/(taille_base + 1));
      printf("Nombre de decompositions trouvées : %i == %i %%", accept, percent);
      printf("\n\n");
    }
    if(accept >= taille_base + 1 || mpz_cmp(y, sqrt_n) == 0){
      sstop = 1;
    }
    mpz_add_ui(y, y, increment);
  }
  free(liste_facteurs);
  mpz_clears(y, resultat, NULL);
}
/*
void mult_complex(struct imag sqr1,  struct imag sqr2,  struct imag res){
  mpf_t temp, temp2_r, temp2_i;
  mpf_inits(temp, temp2_r, temp2_i, NULL);
  mpf_add(temp2_r, sqr1.r, sqr2.r);  
  mpf_add(temp, sqr1.i, sqr2.i);
  mpf_add(temp2_r, res.r, temp);
  mpf_add(temp, sqr1.i, sqr2.r);
  mpf_add(temp2_i, res.i, temp);
  mpf_add(temp, sqr1.r, sqr2.i);
  mpf_add(temp2_i, res.i, temp);
  mpf_set(res.r, temp2_r);
  mpf_set(res.i, temp2_i);
  mpf_clears(temp, temp2_r, temp2_i, NULL);
}

void racine_de_f(mpz_t N, long p_e, long* R, long p, int e){
  mpz_t a, x, r1, i1, xb, pow, prime, prime_c, ran, mod, N_4;
  int alea;
  int legendre = 1;
  //gmp_randstate_t state;
  //gmp_randinit_default(state);
  mpz_inits(r1, i1, a, x, xb, prime_c ,prime, ran, pow, mod, N_4 ,NULL);
  mpz_set_ui(prime, p);
  mpz_set_ui(pow, e);
  mpz_set(N_4, N);
  mpz_mul_ui(N_4, N_4, 4);
  mpf_set_default_prec()
  if(e == 1){
    while(legendre != -1){
      //mpz_urandomm(ran, state, prime);
      alea = rand() % p + 1;
      mpz_set_ui(ran, alea);
      //printf("p = %li\n", p);
      //gmp_printf("ran = %Zd ", ran);
      mpz_set(a, ran);
      mpz_mul(ran, ran, ran);
      mpz_sub(ran, ran, N_4);
      //gmp_printf("a² - N = %Zd\n\n", ran);
      legendre = mpz_legendre(ran, prime);
    }
    struct imag sqr;
    mpf_inits(sqr.r, sqr.i, NULL);
    mpf_set_z(sqr.r, a);
    mpf_set(sqr.i, i1);
    //printf("%i\n", mpz_cmp(ran, 0));
    if(mpf_cmp_ui(ran, 0) < 0){
      mpz_abs(ran, ran);
      mpf_set_z(sqr.i, ran);
    }
    else{
      mpf_set_z(sqr.i, ran);
    }
    mpz_sqrt(sqr.i, sqr.i);
    int i;
    struct imag res;
    mpf_inits(res.r, res.i, NULL);
    mpf_set(res.r, sqr.r);
    mpf_set(res.i, sqr.i);
    for(i = 1; i <= ((p+1)/2); i++){
      mult_complex(res, sqr, res);
    }
    /*mpz_sqrt(ran, ran);
    mpz_mul(prime_c, prime, prime);
    mpz_add(x, a, ran);
    mpz_powm_ui(x, x, (p+1)/2, prime_c);
    mpz_set_f(x, res.r);
    mpz_mul(mod, x, x);
    mpz_mul(xb, x, a);
    mpz_sub(mod, mod, xb);
    mpz_add(mod, mod, N);
    // mpz_mod(x, x, mod);
    //mpz_mod_ui(x, x, (p*p));
    R[0] = (mpz_get_ui(x) % p_e);
    //mpz_neg(x,x);
    //mpz_mod_ui(x, x, p);
    //R[1] = mpz_get_ui(x);
    R[1] = 0;
    R[2] = 0;
    R[3] = 0;
  }
  if(e >= 2){
    long r;
    mpz_t s_mp;
    long s;
    long k;
    long* R_bis = malloc(sizeof(long) * 4);
    mpz_init(s_mp);
    p_e = p_e / p;
    printf("p_e = %li \n", p_e);
    racine_de_f(N, p_e, R_bis,  p,  e-1);
    r = R_bis[0];
    r = r * r; 
    mpz_sub_ui(s_mp, N, r);
    s = mpz_get_ui(s_mp);
    s = s / (p_e / p);
    k = ((1/2) * s) % p;
    R[0] = (R_bis[0] + (k * (p_e/p))) % p_e;
    //R[1] = (-(R_bis[0] + (k * (p_e/p)))) % p;
    R[2] = 0;
    R[3] = 0;
    R[1] = 0;
    free(R_bis);
    mpz_clear(s_mp);
  }
  mpz_clears(a, x, prime_c ,prime, pow, ran, NULL);
  //gmp_randclear(state);
}

void racine_de_f_2(mpz_t N, long p_e, long* R, long p, int e){
  mpz_t res;
  mpz_t r_2;
  mpz_t v_2;
  long* R_bis = malloc(sizeof(long) * 4);
  if(p_e == 1){
    R[0] = 1;
    R[1] = 0;
    R[2] = 0;
    R[3] = 0;
    return;
  }
  mpz_inits(res, r_2, v_2, NULL);
  if(p_e == 2){
    mpz_mod_ui(res, N, 4);
    if(mpz_cmp_ui(res, 1) == 0){
      R[0] = 1;
      R[1] = 0;
      R[2] = 0;
      R[3] = 0;
      mpz_clears(res, r_2, v_2, NULL);
      return;
    }
  }
  if(p_e >= 8){
    mpz_mod_ui(res, N, 8);
    if(mpz_cmp_ui(res, 1) == 0){
      racine_de_f_2(N, p_e/2, R_bis, p, e-1);
      long r = R_bis[0];
      while(1){
	mpz_set_ui(r_2, r);
	mpz_set_ui(v_2, p_e);
        mpz_powm_ui(r_2, r_2, 2, v_2);
	if(mpz_cmp(r_2, N) == 0){
	  R[0] = r;
	  //R[1] = (-r) % p_e;
	  mpz_cdiv_q_ui(v_2, v_2, 2);
	  R[1] = r + mpz_get_ui(v_2);
	  //R[3] = (-r + mpz_get_ui(v_2)) % p;
	  R[2] = 0;
	  R[3] = 0;
	  mpz_clears(res, r_2, v_2, NULL);
	  free(R_bis);
	  return;
	}
	else{
	  mpz_cdiv_q_ui(v_2, v_2, 2);
	  mpz_cdiv_q_ui(v_2, v_2, 2);
	  r = r + mpz_get_ui(v_2);
	}
      }
    } 
  }
  mpz_clears(res, r_2, v_2, NULL);
}

int check_sup(long* R, int T){
  int i = 0;
  while(R[i] != 0){
    printf("R[%i] = %li\n", i, R[i]);
    if(R[i] < T) return i;
    i++;
  }
  return -1;
}

void boucle_criblage(mpz_t sqrt_n, mpz_t N, int taille_base){
  srand(time(NULL));
  int x, i = 0, e = 1, j = 0, k = 2;
  long p, w;
  long* R = malloc(sizeof(long) * 4);
  int T = 15;
  mpz_t v[T];
  //int K = (int)((T/2) * log(T));
  int K = 4;
  for(x = 0; x < T; x++){
    mpz_init2(v[x], 256);
    mpz_set_ui(v[x], 1);
  }
  for(i = 0; i < K; i++){
    p = base_nb_premiers[i];
    printf("p = %li\n", p);
    e = 1;
    while(1){
      R[0] = 0;
      R[1] = 0;
      R[2] = 0;
      R[3] = 0;
      printf("p = %li e = %i pow = %li \n\n", p, e, (long)pow(p, e));
      if(p != 2){
	racine_de_f(N, pow(p, e), R, p, e);
      }
      else{
	racine_de_f_2(N, pow(p, e), R, p, e);
      }
      //printf("racine de N mod %li^%i =  %i\n", p, e, (int)R[0]);
      w = check_sup(R, T);
      if(w == -1) break;
      if(p != 2) k = 1;
      for(j = 0; j < k; j++){
	w = R[j];
	printf("j = %i w = %li , p^e = %li^%i, T = %i\n", j, w, p, e, T);  
	if(w != 0){
	  while(w < T){
	    mpz_mul_ui(v[w], v[w], p);
	    w = w + pow(p, e);
	  }
	}
	else{
	  break;
	}
      }
      e++;
    }
  }
  mpz_t resultat;
  mpz_t y;
  int percent;
  int* liste_facteurs = (int*)calloc(taille_base, sizeof(int));
  mpz_inits(resultat, y, NULL);
  for(x = 0; x < T; x++){
    mpz_set_ui(y, x);
    polyn(sqrt_n, y, N, resultat);
    if(mpz_cmp(v[x], resultat) == 0){
      if(fact(resultat, taille_base, liste_facteurs) == 0){
	accept++;
        gmp_printf("y = %Zd | f(y) = %Zd \n\n", y, resultat);
	affiche_tab(liste_facteurs, base_nb_premiers, taille_base);
	percent = (int)((accept * 100)/(taille_base + 1));
	printf("Nombre de decompositions trouvées : %i == %i %%", accept, percent);
	printf("\n\n");
      } 
    }
    free(R);
    mpz_clears(resultat, y, NULL);
    mpz_clear(v[x]);
  }  
}
*/
void *fonction_thread(void* args){
  struct arg_struc* arg = (struct arg_struc *)args;
  //boucle_criblage(racine_n, n, arg -> taille_base);
  boucle_facteurs(arg -> depart, arg -> taille_base, racine_n,  n, arg -> prod_chinois, arg -> id);
  return NULL;
}

double algCrible(int opt_t, int opt_o, double max_base, mpz_t rez){
  int i = 0, j = 0,taille_base = 0;
  double startTime, endTime;
  long* liste_premiers = prime_list();
  mpz_t n_sub_racine, y;
  int prod_chinois, elmt_base1, elmt_base2, k; //pour l'optimisation avec les reste chinois
  int chinese[4];
  int base[2];
  int* tmp = malloc(sizeof(int) * 2);
  int* module1;
  int* module2;
  base_nb_premiers = NULL;
  mpz_inits(racine_n, n_sub_racine, y, NULL);
  if(opt_t){
    startTime = getCPUTime();
  }
  //nb_thread = omp_get_max_threads();
  printf("Ce processeur a 4 threads qui vont disponible être utilisées\n");
  //omp_set_num_threads(nb_thread);
  mpz_sqrt(racine_n, n); //calcul de la racine de n
  gmp_printf("[Racine(n)] = %Zd\n", racine_n);
  mpz_sub(n_sub_racine, n, racine_n); // calcul de n - sqrt(n)
  base_nb_premiers = get_base(liste_premiers, n, max_base); // calcul de la liste de nombre premier residu quadratique avec n
  printf("Base des nombres premiers choisis : ");
  while(base_nb_premiers[i]){
    //printf("%ld ", base_nb_premiers[i]);
    i++;
  }
  taille_base = i;
  printf("\n\nTaille de la base %i\n", taille_base);
  if(opt_o){
    k = 0;
    elmt_base1 = base_nb_premiers[1];
    elmt_base2 = base_nb_premiers[2];
    prod_chinois  = elmt_base1 * elmt_base2;
    memset(chinese, 0, 4 * sizeof(int));
    base[0] = elmt_base1;
    base[1] = elmt_base2;
    module1 = test_module(elmt_base1, n); // tableau contenant i et j tels que iÂ² mod elmt_base1 = jÂ² mod elmt_base1 = n mod elmt_base1
    module2 = test_module(elmt_base2, n);
    if(module1 == NULL || module2 == NULL){
      printf("problème avec le calcul des module\n");
      exit(1);
    }
    for(i = 0; i < 2; i++){
      for(j = 0; j < 2; j++){
	tmp[0] = module1[i];
	tmp[1] = module2[j];
	chinese[k] = chinese_remainder(base, tmp, 2);
	k++;
      }
    }
    free(tmp);
    printf("Resultats obtenus grâce à  l'optimisation par le théorème chinois : %i | %i | %i | %i\n", chinese[0], chinese[1], chinese[2], chinese[3]);
  }
  sstop = 0;
  accept = 0;
  pthread_t th1, th2, th3, th4;
  struct arg_struc arg1, arg2, arg3, arg4;
  arg1.taille_base = taille_base;
  arg2.taille_base = taille_base;
  arg3.taille_base = taille_base;
  arg4.taille_base = taille_base;
  
  arg1.id = 1;
  arg2.id = 2;
  arg3.id = 3;
  arg4.id = 4;
  /*
    int depart;
    int taille_base;
    int prod_chinois;
    int id;
    }*/  
  //int id = omp_get_thread_num();
  if(opt_o){
    arg1.prod_chinois = prod_chinois;
    arg2.prod_chinois = prod_chinois;
    arg3.prod_chinois = prod_chinois;
    arg4.prod_chinois = prod_chinois;
    arg1.depart = chinese[0];
    arg2.depart = chinese[1];
    arg3.depart = chinese[2];
    arg4.depart = chinese[3];
    pthread_create(&th1, NULL, fonction_thread, &arg1);
    pthread_create(&th2, NULL, fonction_thread, &arg2);
    pthread_create(&th3, NULL, fonction_thread, &arg3);
    pthread_create(&th4, NULL, fonction_thread, &arg4);
  }
  else{
    arg1.prod_chinois = 1;
    arg2.prod_chinois = 4;
    arg3.prod_chinois = 4;
    arg4.prod_chinois = 4;
    arg1.depart = 1;
    arg2.depart = 2;
    arg3.depart = 3;
    arg4.depart = 4;    
    pthread_create(&th1, NULL, fonction_thread, &arg1);
    pthread_create(&th2, NULL, fonction_thread, &arg2);
    pthread_create(&th3, NULL, fonction_thread, &arg3);
    pthread_create(&th4, NULL, fonction_thread, &arg4);
  }
  pthread_join(th1, NULL);
  pthread_join(th2, NULL);
  pthread_join(th3, NULL);
  pthread_join(th4, NULL);
  if(accept >= taille_base + 1){
    if(opt_t){
      endTime = getCPUTime();
      printf("CPU time used = %lf s\n", (endTime - startTime));
    }
    printf("Base de taille adéquate, nombre de décompositions : %i | taille %i\n", accept, taille_base);
  }
  else{
    printf("Base insuffisante\n");
  }
  //free(chinese);
  free(base_nb_premiers);
  free(liste_premiers);
  mpz_clears(racine_n, n_sub_racine, y, NULL);
  if(opt_t){
    return (endTime - startTime);
  }
  return 0;
}

void crible(struct RSA* RSA){
  srand(time(NULL));
  mpz_t rez;
  int repet, incr, ligne, i;
  int nb;
  double tot = 0;
  long max_base;
  mpz_inits(n, rez, NULL);
  if(RSA -> l_option){
    repet = RSA -> l_option;
    printf("Vous avez demandé %i repetition\n", repet);
  }
  else repet = 1;
  incr = 2;
  if(RSA -> d_option){
    ligne = RSA -> d_option;
    printf("Début des entiers choisi ligne %i du fichier\n", ligne);
  }
  else ligne = 3;
  printf("\n<--------- Debut des calculs --------->\n\n");
  for(i = 0; i < repet; i++){
    file_to_mpz(*(RSA -> argv), 10, ligne, n);
    ligne += incr;
    nb = (int)mpz_sizeinbase(n, 10);
    max_base = RSA -> b_option;
    gmp_printf("n = %Zd\n", n);
    gmp_printf("Valeur max de la base = %ld\n", max_base);
    printf("Nombre de chiffre dans n : %i\n", nb);
    tot += algCrible(RSA -> t_option, RSA -> o_option, max_base, rez);
    printf("\n\n");
  }
  printf("<--------- Fin des calculs --------->\n\n");
  tot /= repet;
  if(RSA -> t_option){
    printf("Moyenne du temps CPU utilisé pour le calcul : %lf s\n", tot);
  }
  mpz_clears(n, rez, NULL);
}
