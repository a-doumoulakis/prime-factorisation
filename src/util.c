//
//  util.c
//  RSA
//
//  Created by Wintermute on 07/03/2015.
//  Copyright (c) 2015 Wintermute. All rights reserved.
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include "RSA.h"
#define TAILLE_BUFF 811172
#define TAILLE_BUFF2 100008


static const char *short_options = "nrctol:d:a:b:"; // ajouter ':' apres les options qui on besoin d'argument



int Getopt(struct RSA *RSA){//regarde si les options sont bien mises et si les options qui ont besoin d'argument en on bien.
    enum { state_start = 0, state_next_word, state_short};
    static int state = state_start;
    
    char opt ='?';
    static char *opt_word;
    
    const char *p;
    
    int required = 0;
    
    if (state == state_start) {
        ++RSA->argv;
        --RSA->argc;
        if (*RSA->argv == NULL)return (-1);
        
        if (RSA->argv[0][0] == '-') {
            state = state_next_word;
        }
        else return (-1);
    }
    
    if (state == state_next_word) {
        if (RSA->argv[0] == NULL) return (-1);
        if (RSA->argv[0][0] != '-') return (-1);
        opt_word = *RSA->argv++;
        --RSA->argc;
        state = state_next_word;
        ++opt_word;
    }
    opt = *opt_word++;

    p = strchr(short_options, opt);
    if (p == NULL){
      return (-1);
    }    
    if (p[1] == ':') required = 1;
    
    if (required) {
        if (opt_word[0] == '\0') {
            opt_word = *RSA->argv;
            
            if (opt_word == NULL) {
                printf("Option -%c requires an argument\n",opt);
                return ('?');
            }
            ++RSA->argv;
            --RSA->argc;
            RSA->optarg = opt_word;
            //return opt;
        }
        else{
            
            state = state_next_word;
            RSA->optarg = opt_word;
            
        }
    }
    return opt; 
}

long* prime_list(){
  FILE* file = fopen("new.txt", "r");
  char* buffer = malloc(TAILLE_BUFF * sizeof(char));
  long* result = malloc(TAILLE_BUFF2 * sizeof(long)); 
  char* token;
  int i = 0;
  fgets(buffer, TAILLE_BUFF, file);
  token = strtok(buffer, " ");
  result[i++] = atol(token);
  while(token != NULL){
    token = strtok(NULL, " ");
    if(token){
      result[i++] = atol(token);
    }
  }
  free(buffer);
  fclose(file);
  return result;
}
