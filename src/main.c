//
//  main.c
//  RSA
//
//  Created by Wintermute on 07/03/2015.
//  Copyright (c) 2015 Wintermute. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "RSA.h"

void set_mode(int opt, char *optstr ,struct RSA *RSA){
  if (RSA->mode!=0) {
    printf("can't specify both %s and %s\n", RSA->modestr, optstr);
    exit(1);
  }
    
  RSA->mode=opt;
  RSA->modestr=optstr;    
}


int main(int argc, char* argv[]){
  struct RSA *RSA, RSA_storage;
  int opt;
  //int mode;
  //char *archname;
    
  RSA = &RSA_storage;
  memset(RSA, 0, sizeof(*RSA));
    
  RSA -> argc_orig = argc;
  RSA -> argv_orig = argv;
    
    
  RSA -> argv = argv;
  RSA -> argc = argc;
    
    
  while((opt=Getopt(RSA))!= -1){
    switch(opt){
    case 'n':// algo naif
      set_mode(opt, "-n", RSA);              
      break;
                
    case 'r':// algo rho de pollard
      set_mode(opt, "-r", RSA);
      break;
          
    case 'c':// crible quadratique
      set_mode(opt, "-c", RSA);  
      break;

    case 't':
      RSA->t_option = 1;
      break;
            
      /*case 'C':
	RSA->chdir= RSA->optarg;
	RSA->optarg++;
	break;*/
    case 'o':
      RSA -> o_option = 1;
      break;

    case 'l':
      RSA -> l_option = atoi(RSA -> optarg);
      RSA -> optarg++;
      break;
      
    case 'd':
      RSA -> d_option = atoi(RSA -> optarg);
      RSA -> optarg++;
      break;

    case 'a':
      RSA -> a_option = atoi(RSA -> optarg);
      RSA -> optarg++;
      break;

    case 'b':
      RSA -> b_option = atoi(RSA -> optarg);
      RSA -> optarg++;
      break;

    case '?':
      printf("Message d'erreur\n");
      exit(0);
      break;
      
    default: 
      printf("Autre message d'erreur\n");
      exit(0);
      break;
    }
  }
    
    
  if(RSA->mode=='\0'){
    printf("Must specify one of -r, -n\n");
    exit(0);
  }
    
    
  switch(RSA->mode){

  case 'n':
    naif(RSA);
    break;
  case 'r':
    rho(RSA);
    break;
  case 'c':
    crible(RSA);
    break;
  }   

  // cleanup_exclusions(RSA);
  return 0; 
}
